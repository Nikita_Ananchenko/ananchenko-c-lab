﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phone_Book
{
    class Program
    {
        static int i = 0;
        static int h = 0;
        static void Main(string[] args)
        {
            Dictionary<int, Direct> book = new Dictionary<int, Direct>();
            bool flag = true;
            while (flag)
            {
                Console.WriteLine("-----Приветствую вас! Эта программа реализует функции записной телефонной книги.-----");
                Console.WriteLine("Главное меню. Выберите то, что хотите сделать.");
                Console.WriteLine("1. Создание новой записи");
                Console.WriteLine("2. Редактирование созданных записей");
                Console.WriteLine("3. Удаление созданных записей");
                Console.WriteLine("4. Просмотр созданных учетных записей");
                Console.WriteLine("5. Просмотр всех созданных учетных записей с краткой информацией");
                Console.WriteLine("6. Выход из программы");
                int q = int.Parse(Console.ReadLine());
                switch (q)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Вы создали " + i + " учетных записей. Заполните следующие поля информации для нового человека:");
                        Console.WriteLine("1.Фамилия; 2.Имя; 3.Отчество; 4.Номер телефона; 5.Страна; 6.Дата рождения; 7.Организация; 8.Должность; 9.Прочие заметки");   
                        book[i]=new Direct(Console.ReadLine(), Console.ReadLine(), Console.ReadLine(), long.Parse(Console.ReadLine()), Console.ReadLine(), Console.ReadLine(), Console.ReadLine(), Console.ReadLine(), Console.ReadLine());
                        i++;
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 2:
                        h = 0;
                        Console.Clear();
                        Console.WriteLine("Вот все ваши контакты. Выберите id контакта для его изменения.");
                        Console.WriteLine(" ");
                        foreach (Direct p in book.Values)
                        {
                            Console.WriteLine("Контакт " + h + " в телефонной книге");
                            Console.WriteLine("Фамилия: " + p.surname);
                            Console.WriteLine("Имя: " + p.name);
                            Console.WriteLine("Отчество: " + p.patronymic);
                            Console.WriteLine("Номер телефона: " + p.phoneNumber);
                            Console.WriteLine("Страна: " + p.country);
                            Console.WriteLine("Дата рождения: " + p.birthday);
                            Console.WriteLine("Организация: " + p.organization);
                            Console.WriteLine("Должность: " + p.position);
                            Console.WriteLine("Прочие заметки: " + p.notes);
                            Console.WriteLine(" ");
                            h++;
                        }
                        int z = int.Parse(Console.ReadLine());
                        bool fl = true;
                        while(fl)
                        {
                            Console.WriteLine("1.Изменить фамилию");
                            Console.WriteLine("2.Изменить имя");
                            Console.WriteLine("3.Изменить отчество");
                            Console.WriteLine("4.Изменить номер телефона");
                            Console.WriteLine("5.Изменить страну");
                            Console.WriteLine("6.Изменить дату рождения");
                            Console.WriteLine("7.Изменить организацию");
                            Console.WriteLine("8.Изменить должность");
                            Console.WriteLine("9.Изменить прочие заметки");
                            Console.WriteLine("10.Закончить измения");
                            int sw = int.Parse(Console.ReadLine());
                            switch(sw)
                            {
                                case 1:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новую фамилию");
                                        book.ElementAt(z).Value.surname = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 2:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новое имя");
                                        book.ElementAt(z).Value.name = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 3:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новое отчество");
                                        book.ElementAt(z).Value.patronymic = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 4:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новый номер телефона");
                                        book.ElementAt(z).Value.phoneNumber = long.Parse(Console.ReadLine());
                                        Console.Clear();
                                        break;
                                    }
                                case 5:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новую страну");
                                        book.ElementAt(z).Value.country = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 6:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новую дату рождения");
                                        book.ElementAt(z).Value.birthday = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 7:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новую организацию");
                                        book.ElementAt(z).Value.organization = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 8:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новую должность");
                                        book.ElementAt(z).Value.position = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 9:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новые прочие заметки");
                                        book.ElementAt(z).Value.notes = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 10:
                                    {
                                        Console.WriteLine("Изменения завершены");
                                        fl = false;
                                        break;
                                    }
                            }             
                        }
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        h = 0;
                        Console.Clear();
                        Console.WriteLine("Вот все ваши контакты. Выберите id контакта для его удаления.");
                        Console.WriteLine(" ");
                        foreach (Direct p in book.Values)
                        {
                            Console.WriteLine("Контакт " + h + " в телефонной книге");
                            Console.WriteLine("Фамилия: " + p.surname);
                            Console.WriteLine("Имя: " + p.name);
                            Console.WriteLine("Отчество: " + p.patronymic);
                            Console.WriteLine("Номер телефона: " + p.phoneNumber);
                            Console.WriteLine("Страна: " + p.country);
                            Console.WriteLine("Дата рождения: " + p.birthday);
                            Console.WriteLine("Организация: " + p.organization);
                            Console.WriteLine("Должность: " + p.position);
                            Console.WriteLine("Прочие заметки: " + p.notes);
                            Console.WriteLine(" ");
                            h++;
                        }
                        int l = int.Parse(Console.ReadLine());
                        book.Remove(l);
                        Console.WriteLine("Запись удалена. Нажмите любую клавишу, чтобы продолжить.");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 4:
                        h = 0;
                        Console.Clear();
                        Console.WriteLine("Вот список всех ваших контактов в телефонной книге с краткой информацией о каждом:");
                        Console.WriteLine(" ");
                        foreach (Direct p in book.Values)
                        {

                            Console.WriteLine("Контакт " + h + " в телефонной книге");
                            Console.WriteLine("Фамилия: " + p.surname);
                            Console.WriteLine("Имя: " + p.name);
                            Console.WriteLine("Номер телефона: " + p.phoneNumber);
                            Console.WriteLine(" ");
                            h++;
                        }
                        Console.WriteLine("Выберите, кого хотите посмотреть.");
                        int m = int.Parse(Console.ReadLine());
                        Console.Clear();
                        Console.WriteLine("Контакт " + m + " в телефонной книге");
                        Console.WriteLine(" ");
                        Console.WriteLine("Фамилия: "+ book.ElementAt(m).Value.surname);
                        Console.WriteLine("Имя: "+ book.ElementAt(m).Value.name);
                        Console.WriteLine("Отчество: " + book.ElementAt(m).Value.patronymic);
                        Console.WriteLine("Номер телефона: " + book.ElementAt(m).Value.phoneNumber);
                        Console.WriteLine("Страна: " + book.ElementAt(m).Value.country);
                        Console.WriteLine("Дата рождения: " + book.ElementAt(m).Value.birthday);
                        Console.WriteLine("Организация: " + book.ElementAt(m).Value.organization);
                        Console.WriteLine("Должность: " + book.ElementAt(m).Value.position);
                        Console.WriteLine("Прочие заметки: " + book.ElementAt(m).Value.notes);
                        Console.WriteLine(" ");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 5:
                        h = 0;
                        Console.Clear();
                        Console.WriteLine("Вот список всех ваших контактов в телефонной книге с краткой информацией о каждом:");
                        Console.WriteLine(" ");
                        foreach (Direct p in book.Values)
                        {                           
                            Console.WriteLine("Контакт "+h+" в телефонной книге");
                            Console.WriteLine("Фамилия: " + p.surname);
                            Console.WriteLine("Имя: " + p.name);
                            Console.WriteLine("Номер телефона: " + p.phoneNumber);
                            Console.WriteLine(" ");
                            h++;
                        }
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 6:
                        Console.Clear();
                        Console.WriteLine("Всего хорошего!");
                        flag = false;
                        break;
                }
            }
            Console.ReadKey();
        }
    }
    class Direct
    {
        public string surname;
        public string name;
        public string patronymic;
        public long phoneNumber;
        public string country;
        public string birthday;
        public string organization;
        public string position;
        public string notes;
        public Direct(string surname,string name,string patronymic,long phoneNumber, string country,string birthday, string organization,string position, string notes)
        {
            this.surname = surname;
            this.name = name;
            this.patronymic = patronymic;
            this.phoneNumber = phoneNumber;
            this.country = country;
            this.birthday = birthday;
            this.organization = organization;
            this.position = position;
            this.notes = notes; 
        }
    }   
}
